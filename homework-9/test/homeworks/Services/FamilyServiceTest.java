package homeworks.Services;

import homeworks.*;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.LinkedHashMap;

class FamilyServiceTest {

    private final FamilyService familyService = new FamilyService();

    Man dad = new Man("Derek", "Thompson", 1975, 99,
            new LinkedHashMap<String, String>() {{
                put(DayOfWeek.SUNDAY.name(), "sleep all day long");
                put(DayOfWeek.MONDAY.name(), "go to work");
                put(DayOfWeek.TUESDAY.name(), "read book");
                put(DayOfWeek.WEDNESDAY.name(), "go to the swimming pool");
                put(DayOfWeek.THURSDAY.name(), "go to kfc");
                put(DayOfWeek.FRIDAY.name(), "participate in music");
                put(DayOfWeek.SATURDAY.name(), "play video games");
            }}
    );

    Woman mom = new Woman(
            "Natalie", "Portman", 1979, 99,
            new LinkedHashMap<String, String>() {{
                put(DayOfWeek.SUNDAY.name(), "sleep all day long");
                put(DayOfWeek.MONDAY.name(), "go to work");
                put(DayOfWeek.TUESDAY.name(), "do some stuff");
                put(DayOfWeek.WEDNESDAY.name(), "go to the market");
                put(DayOfWeek.THURSDAY.name(), "go to kfc");
                put(DayOfWeek.FRIDAY.name(), "participate in music");
                put(DayOfWeek.SATURDAY.name(), "play with doggy");
            }}
    );

    Woman daughter = new Woman(
            "Susan", "Docker", 2008, 99, new LinkedHashMap<>()
    );

    Family fam = new Family(mom, dad);


    @Test
    void getAllFamiliesTest() {
        String expectedFamilies = "[Family info - { mother=Human{name=Natalie, surname=Portman, year=1979, iq=99," +
                " schedule={SUNDAY=sleep all day long, MONDAY=go to work, TUESDAY=do some stuff, WEDNESDAY=go to the market," +
                " THURSDAY=go to kfc, FRIDAY=participate in music, SATURDAY=play with doggy}}," +
                " father=Human{name=Derek, surname=Thompson, year=1975, iq=99, schedule={SUNDAY=sleep all day long," +
                " MONDAY=go to work, TUESDAY=read book, WEDNESDAY=go to the swimming pool, THURSDAY=go to kfc," +
                " FRIDAY=participate in music, SATURDAY=play video games}}, child=[], pet=[]}]";
        Assert.assertEquals(expectedFamilies, familyService.getAllFamilies().toString());
    }

    private final ByteArrayOutputStream output = new ByteArrayOutputStream();

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(output));
    }


    @Test
    void displayAllFamiliesTest() {
        String expectedFamilies = "1. Family info - { mother=Human{name=Natalie, surname=Portman, year=1979, iq=99," +
                " schedule={SUNDAY=sleep all day long, MONDAY=go to work, TUESDAY=do some stuff, WEDNESDAY=go to the market," +
                " THURSDAY=go to kfc, FRIDAY=participate in music, SATURDAY=play with doggy}}, father=Human{name=Derek," +
                " surname=Thompson, year=1975, iq=99, schedule={SUNDAY=sleep all day long, MONDAY=go to work, TUESDAY=read book," +
                " WEDNESDAY=go to the swimming pool, THURSDAY=go to kfc, FRIDAY=participate in music, SATURDAY=play video games}}," +
                " child=[], pet=[]}\n";

        familyService.getAllFamilies().forEach(e -> System.out.println((familyService.getAllFamilies().indexOf(e) + 1) + ". " + e));

        Assert.assertEquals(expectedFamilies, output.toString());
    }

    @Test
    void getFamiliesBiggerThanTest() {
        String expectedFamilies = "Family info - { mother=Human{name=Natalie, surname=Portman, year=1979, iq=99," +
                " schedule={SUNDAY=sleep all day long, MONDAY=go to work, TUESDAY=do some stuff, WEDNESDAY=go to the market," +
                " THURSDAY=go to kfc, FRIDAY=participate in music, SATURDAY=play with doggy}}," +
                " father=Human{name=Derek, surname=Thompson, year=1975, iq=99, schedule={SUNDAY=sleep all day long," +
                " MONDAY=go to work, TUESDAY=read book, WEDNESDAY=go to the swimming pool, THURSDAY=go to kfc," +
                " FRIDAY=participate in music, SATURDAY=play video games}}, child=[Human{name=Susan, surname=Docker, year=2008, iq=99, schedule={}}], pet=[]}\n";
        familyService.adoptChild(fam, daughter);
        familyService.getFamiliesBiggerThan(2);
        Assert.assertEquals(expectedFamilies, output.toString());
    }

    @AfterEach
    public void cleanUpStreams() {
        System.setOut(null);
    }

    @Test
    void getFamiliesLessThanTest() {
        String expectedFamilies = "Family info - { mother=Human{name=Natalie, surname=Portman, year=1979, iq=99," +
                " schedule={SUNDAY=sleep all day long, MONDAY=go to work, TUESDAY=do some stuff, WEDNESDAY=go to the market," +
                " THURSDAY=go to kfc, FRIDAY=participate in music, SATURDAY=play with doggy}}," +
                " father=Human{name=Derek, surname=Thompson, year=1975, iq=99, schedule={SUNDAY=sleep all day long," +
                " MONDAY=go to work, TUESDAY=read book, WEDNESDAY=go to the swimming pool, THURSDAY=go to kfc," +
                " FRIDAY=participate in music, SATURDAY=play video games}}, child=[], pet=[]}\n";
        familyService.getFamiliesLessThan(3);
        Assert.assertEquals(expectedFamilies, output.toString());
    }

    @Test
    void countFamiliesWithMemberNumberTest() {
        Integer expectedNumber = 1;
        Assert.assertEquals(expectedNumber, familyService.countFamiliesWithMemberNumber(2));
    }

    @Test
    void createNewFamilyTest() {
        int initialSize = familyService.getAllFamilies().size();
        Woman woman = new Woman("Meg", "Rata", 1999, 99, new LinkedHashMap<>());
        Man man = new Man("Tommie", "Leeks", 1997, 99, new LinkedHashMap<>());
        familyService.createNewFamily(woman, man);
        Assert.assertEquals(initialSize + 1, familyService.getAllFamilies().size());
    }

    @Test
    void deleteFamilyByIndexTest() {
        familyService.deleteFamilyByIndex(0);
        Assert.assertFalse(familyService.getAllFamilies().contains(fam));
    }

    @Test
    void bornChildTest() {
        int initialChildrenAmount = fam.getChildren().size();
        familyService.bornChild(fam, "Susan", "Mark");
        Assert.assertEquals(initialChildrenAmount + 1, fam.getChildren().size());
    }

    @Test
    void adoptChildTest() {
        int initialChildrenAmount = fam.getChildren().size();
        familyService.adoptChild(fam, daughter);
        Assert.assertEquals(initialChildrenAmount + 1, fam.getChildren().size());
    }

    @Test
    void deleteAllChildrenOlderThenTest() {
        familyService.adoptChild(fam, daughter);
        int initialChildrenAmount = fam.getChildren().size();
        familyService.deleteAllChildrenOlderThen(10);
        Assert.assertEquals(initialChildrenAmount - 1, fam.getChildren().size());
    }

    @Test
    void countTest() {
        Integer expectedAmount = 1;
        Assert.assertEquals(expectedAmount, familyService.count());
    }

    @Test
    void getFamilyByIdTest() {
        Family expectedFam = familyService.getFamilyById(0);
        Assert.assertEquals(fam, expectedFam);
    }

    @Test
    void getPetsTest() {
        String expectedAnimalSet = "[]";
        Assert.assertEquals(expectedAnimalSet, familyService.getPets(0).toString());
    }

    @Test
    void addPetTest() {
        int initialPetsAmount = familyService.getPets(0).size();
        familyService.addPet(0, new Dog("Rocky", 5, 55, Species.BULLDOG));
        Assert.assertEquals(initialPetsAmount + 1, familyService.getPets(0).size());
    }
}