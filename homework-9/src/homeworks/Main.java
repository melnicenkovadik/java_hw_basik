package homeworks;

import homeworks.Controllers.FamilyController;

import java.util.LinkedHashMap;
import java.util.Random;

public class Main {

    private static int getRandomNum () {
        Random random = new Random();
        return random.nextInt(3);
    }
    public static void main(String[] args) {
        Dog doggy = new Dog("Beebop",
                3, Main.getRandomNum(),
                Species.BULLDOG
        );
        Fish fish = new Fish("Nemo",
                1, Main.getRandomNum(),
                Species.CLOWN
        );
        Man dad = new Man("Robert", "Docker", 1975,  Main.getRandomNum(),
                new LinkedHashMap<String, String>() {{
                    put(DayOfWeek.SUNDAY.name(), "sleep all day long");
                    put(DayOfWeek.MONDAY.name(), "go to work");
                    put(DayOfWeek.TUESDAY.name(), "read book");
                    put(DayOfWeek.WEDNESDAY.name(), "go to the swimming pool");
                    put(DayOfWeek.THURSDAY.name(), "go to kfc");
                    put(DayOfWeek.FRIDAY.name(), "participate in music");
                    put(DayOfWeek.SATURDAY.name(), "play video games");
                }}
                );
        Woman mom = new Woman(
                "Amanda", "Docker", 1979, Main.getRandomNum(),
                new LinkedHashMap<String, String>() {{
                    put(DayOfWeek.SUNDAY.name(), "sleep all day long");
                    put(DayOfWeek.MONDAY.name(), "go to work");
                    put(DayOfWeek.TUESDAY.name(), "do some stuff");
                    put(DayOfWeek.WEDNESDAY.name(), "go to the market");
                    put(DayOfWeek.THURSDAY.name(), "go to kfc");
                    put(DayOfWeek.FRIDAY.name(), "participate in music");
                    put(DayOfWeek.SATURDAY.name(), "play with doggy");
                }}
        );
        Man son = new Man(
                "Andy", "Docker", 2001, Main.getRandomNum(),
                new LinkedHashMap<String, String>() {{
                    put(DayOfWeek.SUNDAY.name(), "sleep all day long");
                    put(DayOfWeek.MONDAY.name(), "go to school");
                    put(DayOfWeek.TUESDAY.name(), "read book");
                    put(DayOfWeek.WEDNESDAY.name(), "play football");
                    put(DayOfWeek.THURSDAY.name(), "go to kfc");
                    put(DayOfWeek.FRIDAY.name(), "participate in music");
                    put(DayOfWeek.SATURDAY.name(), "play video games");
                }}
        );
        Man son2 = new Man(
                "Mikey", "Docker", 2002, Main.getRandomNum(),
                new LinkedHashMap<String, String>() {{
                    put(DayOfWeek.SUNDAY.name(), "sleep all day long");
                    put(DayOfWeek.MONDAY.name(), "go to work");
                    put(DayOfWeek.TUESDAY.name(), "read book");
                    put(DayOfWeek.WEDNESDAY.name(), "play guitar");
                    put(DayOfWeek.THURSDAY.name(), "go to another fast-food");
                    put(DayOfWeek.FRIDAY.name(), "participate in music");
                    put(DayOfWeek.SATURDAY.name(), "play video games");
                }}
        );
        Family fam = new Family(mom, dad);

        Man dad2 = new Man("Derek", "Thompson", 1975,  Main.getRandomNum(),
                new LinkedHashMap<String, String>() {{
                    put(DayOfWeek.SUNDAY.name(), "sleep all day long");
                    put(DayOfWeek.MONDAY.name(), "go to work");
                    put(DayOfWeek.TUESDAY.name(), "read book");
                    put(DayOfWeek.WEDNESDAY.name(), "go to the swimming pool");
                    put(DayOfWeek.THURSDAY.name(), "go to kfc");
                    put(DayOfWeek.FRIDAY.name(), "participate in music");
                    put(DayOfWeek.SATURDAY.name(), "play video games");
                }}
        );
        Woman mom2 = new Woman(
                "Natalie", "Portman", 1979, Main.getRandomNum(),
                new LinkedHashMap<String, String>() {{
                    put(DayOfWeek.SUNDAY.name(), "sleep all day long");
                    put(DayOfWeek.MONDAY.name(), "go to work");
                    put(DayOfWeek.TUESDAY.name(), "do some stuff");
                    put(DayOfWeek.WEDNESDAY.name(), "go to the market");
                    put(DayOfWeek.THURSDAY.name(), "go to kfc");
                    put(DayOfWeek.FRIDAY.name(), "participate in music");
                    put(DayOfWeek.SATURDAY.name(), "play with doggy");
                }}
        );

        Woman daughter = new Woman(
                "Scarlet", "Rosario", 1999, Main.getRandomNum(),
                new LinkedHashMap<String, String>() {{
                    put(DayOfWeek.SUNDAY.name(), "sleep all day long");
                    put(DayOfWeek.MONDAY.name(), "go to work");
                    put(DayOfWeek.TUESDAY.name(), "do some stuff");
                    put(DayOfWeek.WEDNESDAY.name(), "go to the market");
                    put(DayOfWeek.THURSDAY.name(), "go to kfc");
                    put(DayOfWeek.FRIDAY.name(), "participate in music");
                    put(DayOfWeek.SATURDAY.name(), "play with doggy");
                }}
        );

        System.out.println(fam.addChild(son));
        System.out.println(fam.addChild(son2));

        System.out.println(doggy.eat());
        System.out.println(doggy.respond());
        System.out.println(doggy.foul());
        doggy.setHabits("barking");
        doggy.setHabits("long-sleeping");
        System.out.println(doggy.toString());
        System.out.println(fish.toString());

        System.out.println(son.greetPet());
        System.out.println(son.describePet());
        System.out.println(mom.toString());

        System.out.println(mom.hashCode());
        System.out.println(fam.hashCode());
        System.out.println(dad.equals(mom));

        System.out.println(fam.countFamily());
        System.out.println(fam.deleteChild(son));
        System.out.println(fam.countFamily());
        System.out.println(fam.toString());
        System.out.println(fam.deleteChild(0));
        System.out.println(fam.countFamily());
        son2.setFamily(fam);
        System.out.println(fam.toString());
        fam.addPet(doggy);
        fam.addPet(fish);
        System.out.println(fam.getPet());
        System.out.println(fam.getEveryPet("nickname"));
        System.out.println(dad.describePet());
        System.out.println(dad.eatNuggets());
        System.out.println(mom.drinkWine());
        System.out.println(fam.countFamily());
        System.out.println(fam.getChildren());

        System.out.println("------------------------------------------");

        Family fam2 = new Family(mom2, dad2);

        FamilyController familyController = new FamilyController();
        familyController.displayAllFamilies();
        familyController.getFamiliesBiggerThan(3);
        familyController.getFamiliesLessThan(3);
        System.out.println("countFamiliesWithMemberNumber - " + familyController.countFamiliesWithMemberNumber(2));
        familyController.createNewFamily(mom2, dad2);
        System.out.println(familyController.count());
        familyController.deleteFamilyByIndex(0);
        System.out.println(familyController.count());
        System.out.println(familyController.bornChild(fam, "Susan", "Mark"));
        System.out.println(familyController.adoptChild(fam, daughter));
        System.out.println(fam.countFamily());
        familyController.deleteAllChildrenOlderThen(20);
        System.out.println(fam.countFamily());
        System.out.println(familyController.getFamilyById(1));
        System.out.println(familyController.getPets(1));
        familyController.addPet(
                1,
                new Dog("Rocky",
                5, Main.getRandomNum(),
                Species.BULLDOG
        ));
        System.out.println(familyController.getPets(1));

//        for (int i = 0; i < 500000; i++) {
//            new Human("Neo", "Matrix", 1919, Main.getRandomNum(),
//                    new String[][]{
//                            {"Sunday", "sleep all day long"},
//                            {"Monday", "take pill"},
//                            {"Tuesday", "read book"},
//                            {"Wednesday", "go to the swimming pool"},
//                            {"Thursday", "go to kfc"},
//                            {"Friday", "participate in music"},
//                            {"Saturday", "play video games"}
//                    }
//            );
//        }

    }
}