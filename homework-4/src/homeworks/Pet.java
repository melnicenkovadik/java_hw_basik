package homeworks;

import java.util.Arrays;
import java.util.Random;

class Pet {

    String species;
    String nickname;
    int age;
    int trickLevel;
    private String [] habits;

    Pet () {

    }

    Pet (String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    Pet (String species, String nickname, int age, int trickLevel, String [] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    String eat () {
       return "Я кушаю!";
    }

    String respond () {
        return "Привет, хозяин. Я - " + this.nickname + ", я соскучился!";
    }

    String foul () {
        return "Нужно хорошо замести следы...";
    }

    @Override
    public String toString() {
        return this.species + "{nickname=" + this.nickname
                + ", age=" + this.age + ", trickLevel=" +
                this.trickLevel + ", habits=" + Arrays.toString(this.habits) + "}";
    }
}
