package homeworks;

import java.util.Random;

public class Main {

    private static int getRandomNum () {
        Random random = new Random();
        return random.nextInt(101);
    }

    public static void main(String[] args) {
        Human neighbour = new Human();
        Pet neigbourDog = new Pet("Labrador", "Spankeey");
        Pet streetDog = new Pet();
        Pet doggy = new Pet(
                "Bulldog", "Beebop",
                3, Main.getRandomNum(),
                new String[]{"barking", "eating", "sleeping"}
        );
        Human dad = new Human("Robert", "Docker", 1975);
        Human mom = new Human(
                "Amanda", "Docker", 1975,
                new Human("John", "Docker", 1950),
                new Human("Lisa", "Docker", 1953)
        );
        Human son = new Human(
                "Andy", "Docker", 2001, Main.getRandomNum(), doggy, mom, dad,
                new String[][] {
                        {"Sunday", "sleep all day long"},
                        {"Monday", "go to school"},
                        {"Tuesday", "read book"},
                        {"Wednesday", "go to the swimming pool"},
                        {"Thursday", "go to kfc"},
                        {"Friday", "participate in music"},
                        {"Saturday", "play video games"}
                }
        );
        System.out.println(doggy.eat());
        System.out.println(doggy.respond());
        System.out.println(doggy.foul());
        System.out.println(doggy.toString());

        System.out.println(son.greetPet());
        System.out.println(son.describePet());
        System.out.println(son.toString());
    }
}
