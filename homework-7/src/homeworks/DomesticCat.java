package homeworks;

class DomesticCat extends Pet implements Foulable {

    DomesticCat (String nickname, int age, int trickLevel, String [] habits, Species species) {
        super(nickname, age, trickLevel, habits);
        this.species = species;
    };

    String respond () {
        return "Привет, хозяин. Я - " + this.nickname + ", где моя еда?";
    }

    @Override
    public String foul() {
        return "Нужно хорошо замести следы...";
    }
}