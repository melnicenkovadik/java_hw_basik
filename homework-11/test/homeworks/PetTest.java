package homeworks;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class PetTest {

    @Test
    public void testToString() {
        Pet pet = new Dog("Beebop",
                3, 55,
                Species.BULLDOG);
        String expected = "{nickname=Beebop, age=3, trickLevel=55, habits=[], species=BULLDOG}";
        Assert.assertEquals(expected, pet.toString());
    }
}