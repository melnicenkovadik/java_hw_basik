package homeworks;

import java.util.Random;

public class Main {

    private static int getRandomNum () {
        Random random = new Random();
        return random.nextInt(101);
    }
    public static void main(String[] args) {
        Pet doggy = new Pet(
                "Bulldog", "Beebop",
                3, Main.getRandomNum(),
                new String[]{"barking", "eating", "sleeping"}
        );
        Human dad = new Human("Robert", "Docker", 1975,  Main.getRandomNum(),
                new String[][] {
                        {"Sunday", "sleep all day long"},
                        {"Monday", "go to work"},
                        {"Tuesday", "read book"},
                        {"Wednesday", "go to the swimming pool"},
                        {"Thursday", "go to kfc"},
                        {"Friday", "participate in music"},
                        {"Saturday", "play video games"}
                }
                );
        Human mom = new Human(
                "Amanda", "Docker", 1979, Main.getRandomNum(),
                new String[][] {
                        {"Sunday", "sleep all day long"},
                        {"Monday", "go to work"},
                        {"Tuesday", "read book"},
                        {"Wednesday", "go to the swimming pool"},
                        {"Thursday", "go to kfc"},
                        {"Friday", "participate in music"},
                        {"Saturday", "play video games"}
                }
        );
        Human son = new Human(
                "Andy", "Docker", 2001, Main.getRandomNum(),
                new String[][] {
                        {"Sunday", "sleep all day long"},
                        {"Monday", "go to school"},
                        {"Tuesday", "read book"},
                        {"Wednesday", "go to the swimming pool"},
                        {"Thursday", "go to kfc"},
                        {"Friday", "participate in music"},
                        {"Saturday", "play video games"}
                }
        );
        Human son2 = new Human(
                "Mikey", "Docker", 2002, Main.getRandomNum(),
                new String[][] {
                        {"Sunday", "sleep all day long"},
                        {"Monday", "go to school"},
                        {"Tuesday", "read book"},
                        {"Wednesday", "go to the swimming pool"},
                        {"Thursday", "go to kfc"},
                        {"Friday", "participate in music"},
                        {"Saturday", "play video games"}
                }
        );
        Family fam = new Family(mom, dad, doggy);

        fam.addChild(son);

        System.out.println(doggy.eat());
        System.out.println(doggy.respond());
        System.out.println(doggy.foul());
        System.out.println(doggy.toString());

        System.out.println(son.greetPet());
        System.out.println(son.describePet());
        System.out.println(son.toString());

        System.out.println(mom.hashCode());
        System.out.println(fam.hashCode());
        System.out.println(dad.equals(mom));

        System.out.println(fam.countFamily(fam));
        System.out.println(fam.deleteChild(son));
        System.out.println(fam.countFamily(fam));
        System.out.println(fam.toString());
        son2.setFamily(fam);
        System.out.println(fam.toString());
    }
}