package homeworks.DAO;

import homeworks.Family;
import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {
    private static List<Family> families;

    public CollectionFamilyDao() {
        families = new ArrayList<>();
    }

    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return index >= families.size() ? null : families.get(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        int beforeRemoveSize = families.size();
        families.remove(index);
        int afterRemoveSize = families.size();
        return beforeRemoveSize - 1 == afterRemoveSize;
    }

    @Override
    public boolean deleteFamily(Family family) {
        int beforeRemoveSize = families.size();
        families.remove(family);
        int afterRemoveSize = families.size();
        return beforeRemoveSize - 1 == afterRemoveSize;
    }

    @Override
    public void saveFamily(Family family) {
        if (family != null) {
            if (families.contains(family)) {
                families.set(families.indexOf(family), family);
            } else {
                families.add(family);
            }
        }
    }
}
