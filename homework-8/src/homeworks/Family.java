package homeworks;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

public class Family {

    private Human mother;
    private Human father;
    private List<Human> children;
    private HashSet<Pet> pet;
    private Family family;

    Family (Human mother, Human father) {
        this.mother = mother;
        this.mother.setFamily(this);
        this.father = father;
        this.father.setFamily(this);
        this.pet = new HashSet<>();
        this.children = new ArrayList<>();
    }

    public List<Human> getChildren() {
        return children;
    }

    public HashSet<Pet> getPet() {
        return this.pet;
    }

    String getEveryPet(String parameter) {
        StringBuilder allPets = new StringBuilder();

        switch (parameter){
            case "nickname":
                for (Pet pet : this.getPet()) {
                    allPets.append(pet.getNickname()).append("; ");
                }
                break;
            case "trickLevel":
                for (Pet pet : this.getPet()) {
                    allPets.append(pet.getTrickLevel() > 50 ? "очень хитрый; " : "почти не хитрый; ");
                }
                break;
            case "age":
                for (Pet pet : this.getPet()) {
                    allPets.append(pet.getAge()).append("; ");
                }
                break;
            default:
                return "no val";
        }
        return allPets.toString();
    }

    public void addPet(Pet pet) {
        this.pet.add(pet);
    }

    void addChild(Human children) {
        this.children.add(children);
    }

    void deleteChild(Human children) {
        this.children.remove(children);
    }

    void deleteChild(int index) {
        this.children.remove(index);
    }

    int countFamily (Family family) {
        return 2 + (this.pet != null ? this.pet.size() : 0) + this.children.size();
    }

    @Override
    public String toString() {
        return "Family info - { mother=" + this.mother + ", father=" + this.father +
                ", child=" + this.children.toString() + ", pet=" + this.pet.toString() + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return mother.equals(family.mother) &&
                father.equals(family.father) &&
                Objects.equals(children, family.children) &&
                Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, children, pet);
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Deleted obj: " + this.mother + ", " + this.father + "; It's hashcode: " + this.hashCode() );
        super.finalize();
    }
}
