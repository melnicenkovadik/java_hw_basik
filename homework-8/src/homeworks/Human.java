package homeworks;

import java.util.*;

class Human {

    String name;
    String surname;
    int year;
    int iq;
    private Map<String, String> schedule;
    Family family;

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYear() {
        return year;
    }

    public int getIq() {
        return iq;
    }

    Family getFamily() {
        return family;
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }

    void setFamily(Family family) {
        this.family = family;
    }

    Human (String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    Human (String name, String surname, int year, int iq, LinkedHashMap<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    Human (String name, String surname, int year, int iq, LinkedHashMap<String, String> schedule, Family family) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
        this.family = family;
    }

    String greetPet() {
        if (this.family != null && this.family.getPet() != null) {
            return "Привет, " + this.family.getEveryPet("nickname");
        } else {
            return "У меня нет пса";
        }
    }

    String describePet() {
        if (this.family != null && this.family.getPet() != null) {
            return "У меня есть питомец/питомцы: " + this.family.getEveryPet("nickname") +
                    "ему/им: " + this.family.getEveryPet("age") + "он/они: " +
                    this.family.getEveryPet("trickLevel");
        } else {
            return "У меня нет питомца";
        }
    }

    @Override
    public String toString() {
        return "Human{name=" + this.name +
                ", surname=" + this.surname +
                ", year=" + this.year +
                ", iq=" + this.iq +
                ", schedule=" + this.schedule.toString() + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year &&
                iq == human.iq &&
                name.equals(human.name) &&
                surname.equals(human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, iq);
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Deleted obj: " + this.name + ", " + this.surname + "; It's hashcode: " + this.hashCode() );
        super.finalize();
    }
}
