package homeworks;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class FamilyTest {

    @Test
    public void testToString() {
        Family family = new Family( new Human(
         "Amanda", "Docker", 1979, 85,
                new String[][] {
                        {DayOfWeek.SUNDAY.name(), "sleep all day long"},
                        {DayOfWeek.MONDAY.name(), "go to the store"},
                        {DayOfWeek.TUESDAY.name(), "read book"},
                        {DayOfWeek.WEDNESDAY.name(), "go to the swimming pool"},
                        {DayOfWeek.THURSDAY.name(), "go to kfc"},
                        {DayOfWeek.FRIDAY.name(), "participate in music"},
                        {DayOfWeek.SATURDAY.name(), "play piano"}
                }), new Human("Robert", "Docker", 1975,  75,
                new String[][] {
                        {DayOfWeek.SUNDAY.name(), "sleep all day long"},
                        {DayOfWeek.MONDAY.name(), "go to the movie"},
                        {DayOfWeek.TUESDAY.name(), "read book"},
                        {DayOfWeek.WEDNESDAY.name(), "go to the swimming pool"},
                        {DayOfWeek.THURSDAY.name(), "go to kfc"},
                        {DayOfWeek.FRIDAY.name(), "participate in music"},
                        {DayOfWeek.SATURDAY.name(), "play video games"}
                }
        ));
        String expected = "Family info - { " +
                "mother=Human{name=Amanda, surname=Docker, year=1979, iq=85," +
                " schedule=[[SUNDAY, sleep all day long], [MONDAY, go to work]," +
                " [TUESDAY, read book], [WEDNESDAY, go to the swimming pool], [THURSDAY, go to kfc]," +
                " [FRIDAY, participate in music], [SATURDAY, play piano]]," +
                " father=Human{name=Robert, surname=Docker, year=1975, iq=75," +
                " schedule=[[SUNDAY, sleep all day long], [MONDAY, go to work]," +
                " [TUESDAY, read book], [WEDNESDAY, go to the swimming pool]," +
                " [THURSDAY, go to kfc], [FRIDAY, participate in music], [SATURDAY, play video games]]," +
                " child=[], pet=null}";
        Assert.assertEquals(expected, family.toString());
    }

    @Test
    public void testDeleteChild() {
        Family family = new Family(new Human("Big", "Momma"), new Human("Big", "Poppa"));
        Human son = new Human("Ross", "Geller");
        Human son2 = new Human("Chandler", "Bing");
        family.addChild(son);
        family.addChild(son2);
        family.deleteChild(son2);
        family.deleteChild(new Human("Monica", "Bing"));
        Human[] expectedArray = { son };
        Assert.assertArrayEquals(expectedArray, family.getChildren());
    }

    @Test
    public void testAddChild() {
        Family family = new Family(
                new Human("Lilly", "Potter"),
                new Human("James", "Potter")
        );
        Human son = new Human("Harry", "Potter");
        family.addChild(son);
        assertEquals(1, family.getChildren().length);
        assertEquals(son, family.getChildren()[0]);
    }

    @Test
    public void testCountFamily() {
        Family family = new Family(
                new Human("Rhaegar", "Targaryen"),
                new Human("Daenerys", "Targaryen")
        );
        Human son = new Human("Jon", "Snow");
        family.addChild(son);
        assertEquals(3, family.countFamily(family));
    }

}